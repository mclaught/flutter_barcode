import 'dart:convert';
import 'dart:js_util' if(dart.library.io) 'fake_js.dart';
import 'package:barcode_scan2/barcode_scan2.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode/scaners/web_scaner_page.dart';
import 'package:permission_handler/permission_handler.dart';
import 'js_controller.dart';

abstract class Scaner{
  Future<String> scan(BuildContext context);

  Scaner();

  factory Scaner.create(List<String> codeTypes){
    return kIsWeb ? MyScaner(codeTypes) : AndroidScaner();
  }
}

class AndroidScaner extends Scaner{
  @override
  Future<String> scan(BuildContext context) async {
    await Permission.camera.request();

    var result = await BarcodeScanner.scan(options: ScanOptions(
        strings: {
          "cancel": "Отмена",
          "flash_on": "Вкл. вспышку",
          "flash_off": "Выкл. вспышку"
        }
    ));
    print("Scan result: ${result.rawContent}");
    if(result.type != ResultType.Barcode)
      return "";

    return result.rawContent;
  }

}

class WebScaner extends Scaner{
  @override
  Future<String> scan(BuildContext context) async {
    var res = await Navigator.of(context).push(MaterialPageRoute(builder: (context)=>WebScanerPage()));
    print("Scan result: $res");
    return res ?? "";
  }
}

class MyScaner extends Scaner{
  final List<String> codeTypes;

  MyScaner(this.codeTypes);

  @override
  Future<String> scan(BuildContext context)async {
    // var res = await Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MyScanerPage()));
    print("Call js_my_scan");

    List<String> typeList = [];
    for(String type in codeTypes){
      typeList.add(type+"_reader");
    }

    dynamic res = js_my_scan(typeList); //js.context.callMethod("js_my_scan");
    print("js_my_scan = "+res.toString());
    String barcode = await promiseToFuture(res);
    print("Scan result: "+barcode);

    return barcode;
  }
  
}