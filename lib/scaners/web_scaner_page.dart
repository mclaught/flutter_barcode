import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ai_barcode/ai_barcode.dart';

class WebScanerPage extends StatefulWidget{
  @override
  State<WebScanerPage> createState() => _WebScanerPageState();

}

class _WebScanerPageState extends State<WebScanerPage> {
  late ScannerController _scannerController;

  @override
  void initState() {
    _scannerController = ScannerController(scannerResult: scannerResult, scannerViewCreated: (){
      _scannerController.startCamera();
      _scannerController.startCameraPreview();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var scrSz = MediaQuery.of(context).size;
    var wd = scrSz.width < scrSz.height ? scrSz.width : scrSz.height;
    return Scaffold(
      appBar: AppBar(
        title: Text("Сканер"),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(onPressed: (){
                print("Start camera");
                _scannerController.startCamera();
              }, child: Text("StartCam")),
              TextButton(onPressed: (){
                print("Start preview");
                _scannerController.startCameraPreview();
              }, child: Text("StartPreview")),
            ],
          ),
          Center(
            child: Container(
              color: Colors.black26,
              width: wd,
              height: wd,
              child: PlatformAiBarcodeScannerWidget(
                platformScannerController: _scannerController,
                unsupportedDescription: "Не поддерживается",
              ),
            ),
          ),
        ],
      ),
    );
  }

  scannerResult(String result) {
    print("Scan result: "+result);
    _scannerController.stopCameraPreview();
    _scannerController.stopCamera();
    Navigator.of(context).pop(result);
  }
}