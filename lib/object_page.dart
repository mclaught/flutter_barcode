import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode/db/db.dart';
import 'package:flutter_barcode/object_files.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ObjectPage extends StatefulWidget {
  BarcodeObject object;
  final DB db;

  ObjectPage(this.db, this.object);

  @override
  State<ObjectPage> createState() => _ObjectPageState();

}

class _ObjectPageState extends State<ObjectPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.object.name),
      ),
      body: ObjectView(widget.db, widget.object),
    );
  }
}

class ObjectView extends StatelessWidget {
  final DB db;
  final BarcodeObject object;
  final bool showName;
  Function(File file, Uint8List bytes)? onPickFile;
  Function(String descr)? onDescrChanged;
  var _descrController = TextEditingController();

  ObjectView(this.db, this.object, {this.showName=true, this.onPickFile, this.onDescrChanged});//

  @override
  Widget build(BuildContext context) {
    _descrController.text = object.foundDescr;

    List<Widget> list = [];
    if(showName)
      list.add(ParamTile("Название", object.name, Offstage()));
    list.add(ParamTile(object.name, object.description, Icon(Icons.description_outlined)));
    // list.add(Expanded(child: Text(""),));
    list.add(Row(
      children: [
        Container(child: Icon(Icons.edit, color: Colors.grey,), margin: EdgeInsets.only(right: 20),),
        Expanded(child: TextField(
          controller: _descrController,
          onChanged: (String text) => (onDescrChanged ?? (String descr){})(text),
          keyboardType: TextInputType.multiline,
          expands: false,
          minLines: 1,
          maxLines: 4,
        )),
        // IconButton(icon: Icon(Icons.save), onPressed: () async {
        //   object.foundDescr = _descrController.value.text;
        //   print(object.foundDescr);
        //   if(await db.updateDescription(object)) {
        //     FocusScope.of(context).unfocus();
        //     // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //     //   content: Text("Описание сохранено"),
        //     // ));
        //     (onDescrSaved ?? (String descr){})(object.foundDescr);
        //   }
        //
        // },)
      ],
    ));
    list.add(Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, top: 10),
          width: 40, height: 40,
          child: IconButton(onPressed: () async {
            FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.any, );
            if (result != null) {
              if (!kIsWeb) {
                File file = File(result.files.single.path ?? "");
                (onPickFile ?? (file, bytes) {})(file, Uint8List.fromList([]));
              } else {
                (onPickFile ?? (file, bytes) {})(File(result.files.single.name),
                    result.files.single.bytes ?? Uint8List.fromList([]));
              }
            }
          }, icon: Icon(Icons.attach_file), color: Colors.white,),
          decoration: BoxDecoration(color: Theme.of(context).colorScheme.secondary,),
        ),
        Container(
          margin: EdgeInsets.only(left: 5, top: 10),
          width: 40, height: 40,
          child: IconButton(onPressed: () async {
            Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FilesPage(db, object)));
          }, icon: Icon(Icons.image_search), color: Colors.white,),
          decoration: BoxDecoration(color: Theme.of(context).colorScheme.secondary,),
        ),
        Container(
          margin: EdgeInsets.only(left: 5, top: 10),
          width: 40, height: 40,
          child: Icon(object.isChecked ? Icons.check_circle_outline : Icons.circle_outlined),
        ),
        Container(
          margin: EdgeInsets.only(left: 5, top: 10),
          padding: EdgeInsets.only(top: 10),
          height: 40,
          child: Text(object.isChecked ?
            DateFormat("Проверка: dd-MM-yyyy kk:mm").format(object.checkedTime ?? DateTime.fromMillisecondsSinceEpoch(0)) :
            "Не проверено"),
        ),
      ],
    ),);

    return Container(
      padding: EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: list,
      ),
    );
  }
}

class ParamTile extends StatelessWidget {
  final String name;
  final String value;
  final Widget icon;

  ParamTile(this.name, this.value, this.icon);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: Text(name),
      subtitle: Text(value),
    );
  }
}