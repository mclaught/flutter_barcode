import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

abstract class DB {
  static const String url = "https://81.29.133.212:61080/barcode";
  final Uri cmdUri = Uri.parse(url+"/cmd.php");
  final String host = "81.29.133.212";
  final String db_name = "barcode";
  final int port = 61432;
  final String user = "barcode";
  final String pass = "BarCode_Password_19";
  // late PostgreSQLConnection conn;
  // Map<int, Uint8List> _fileCache = {};
  Directory? cacheDir;
  int checkedCnt = 0;
  String session = "";

  StreamController<String> onErrorController = StreamController();
  Stream get onError => onErrorController.stream;

  UserInfo userInfo = UserInfo();
  Project curProject = Project(name: "Проект не выбран");
  List<Project> projects = [];
  List<BarcodeObject> objects = [];
  Map<String, BarcodeObject> objectsIndex = {};

  DB(){

  }

  Future<UserInfo> checkUser(String name, String pass);
  Future<List<Project>> projectList();
  Future<List<BarcodeObject>> objectsList();
  Future<BarcodeObject> findObject(String barcode);
  Future<int> addObject({String name="", String barcode=""});
  Future<bool> checkUserGranted(objectId);
  Future grantUser(BarcodeObject obj, bool checked);
  Future checkObject(BarcodeObject obj);
  Future<bool> addFile(BarcodeObject obj, File file, Uint8List bytes, String descr);
  Future<List<FileInfo>> fileList(BarcodeObject obj);
  Future<Uint8List> fileData(int id);
  Future updateDescription(BarcodeObject obj);

  fileSizeToStr(int fSize) {
    double sz = fSize.toDouble();
    String units = "b";
    if(sz > 1024){
      units = "Kb";
      sz /= 1024;
    }
    if(sz > 1024){
      units = "Mb";
      sz /= 1024;
    }
    if(sz > 1024){
      units = "Gb";
      sz /= 1024;
    }

    String str = "${(sz*1000).round()/1000} $units";
    str = str.replaceAll(".", ",");

    return str;
  }

  Future _createCache()async{
    if(cacheDir == null) {
      cacheDir = await getTemporaryDirectory();
      List<Directory>? dirs = await getExternalCacheDirectories();
      if (dirs != null && dirs.length > 0) {
        cacheDir = dirs[0];
      }
    }
    print(cacheDir);
  }

  void saveFileCache(int id, Uint8List bytes)async {
    await _createCache();
    if(cacheDir != null) {
      String fName = "file_${id}";
      File file = File((cacheDir?.path ?? "") + "/" + fName);
      await file.writeAsBytes(bytes);
    }
  }

  Future<Uint8List> loadFileCache(int id)async{
    await _createCache();
    if(cacheDir != null) {
      String fName = "file_${id}";
      File file = File((cacheDir?.path ?? "") + "/" + fName);
      if(!file.existsSync())
        return Uint8List.fromList([]);

      Uint8List bytes = await file.readAsBytesSync();
      return bytes;
    }
    return Uint8List.fromList([]);
  }
}

class FileInfo {
  final int id;
  final String name;
  final String ext;
  final String description;
  final String size;
  final String session;
  Uint8List bytes;

  FileInfo(this.id, this.name, this.ext, this.description, this.size, this.bytes, this.session);

  Widget image(){
    switch(ext){
      case '.jpg':
      case '.jpeg':
      case '.png':
      case '.gif':
      case '.tiff':
      case '.tif':
        if(bytes.isNotEmpty)
          return Image.memory(bytes, fit: BoxFit.cover,);
        else
          return Image.network(
            DB.url+"/cmd.php?cmd=fileData&fileId=${id}&session=$session",
            headers: {'cookie': "PHPSESSID=$session"},
            fit: BoxFit.cover,
            errorBuilder: (context, _, __) => Image.asset('icon/image.png'),
            loadingBuilder: (context, widget, event) {
              print(event);
              return event!=null ?
                Center(
                  child: SizedBox(
                    width: 50,
                    height: 50,
                    child: CircularProgressIndicator(
                      color: Theme.of(context).colorScheme.secondary,
                    )
                  ),
                ) : widget;
            },
          );

      case '.pdf':
      case '.doc':
      case '.docx':
      case '.txt':
        return Image.asset('icon/doc.png', fit: BoxFit.contain,);

      case '.mp3':
      case '.wav':
      case '.ogg':
      case '.aac':
      return Image.asset('icon/audio.png', fit: BoxFit.contain,);

      default:
        return Image.asset('icon/file.png', fit: BoxFit.contain,);
    }
  }

  ImageProvider imageProvider(){
    switch(ext){
      case '.jpg':
      case '.jpeg':
      case '.png':
      case '.gif':
      case '.tiff':
      case '.tif':
        if(bytes.isNotEmpty)
          return MemoryImage(bytes);
        else
          return NetworkImage(DB.url+"/cmd.php?cmd=fileData&fileId=${id}&session=$session");

      case '.pdf':
      case '.doc':
      case '.docx':
      case '.txt':
        return AssetImage('icon/doc.png');

      case '.mp3':
      case '.wav':
      case '.ogg':
      case '.aac':
        return AssetImage('icon/audio.png');

      default:
        return AssetImage('icon/file.png');
    }
  }

  String url(){
    switch(ext){
      case '.jpg':
      case '.jpeg':
      case '.png':
      case '.gif':
      case '.tiff':
      case '.tif':
          return Uri.parse(DB.url+"/cmd.php?cmd=fileData&fileId=${id}&session=$session").toString();

      case '.pdf':
      case '.doc':
      case '.docx':
      case '.txt':
        return 'icon/doc.png';

      case '.mp3':
      case '.wav':
      case '.ogg':
      case '.aac':
        return 'icon/audio.png';

      default:
        return 'icon/file.png';
    }
  }
}

class UserInfo {
  final int id;
  final String name;

  UserInfo({this.id=-1, this.name=""});

  String toString(){
    return "id=$id name=$name";
  }
}

class Project {
  final int id;
  final String name;
  final String description;

  Project({this.id=-1, this.name="", this.description=""});
}

class BarcodeObject {
  final int id;
  final String name;
  final String description;
  final String barcode;
  bool isChecked;
  DateTime? checkedTime = DateTime.now();
  String foundDescr;
  bool descrChanged = false;

  BarcodeObject({
    this.id=-1,
    this.name="",
    this.description="",
    this.barcode="",
    this.isChecked=false,
    this.checkedTime,
    this.foundDescr=""
  });
}
