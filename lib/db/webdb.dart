import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter_barcode/db/db.dart';
import 'package:http/http.dart';

class WebDB extends DB{
  late HttpClient client;
  String cookie = "";

  WebDB(){
    HttpOverrides.global = DevHttpOverrides();
  }

  Future<dynamic> _post(dynamic data)async{
    print("cookie before: $cookie");

    if(data['cmd'] != 'checkUser') {
      data['session'] = session;
    }
    print("Req: ${data}");
    var result = await post(cmdUri, body: data, headers: {'cookie': cookie});

    if(data['cmd'] == 'checkUser') {
      _getCookie(result.headers['set-cookie'] ?? "");
    }

    if(result.statusCode < 300){
      print("Res body: ${result.body}");
      var obj = jsonDecode(result.body);
      print("Obj: ${obj}");
      return obj;
    }else{
      throw Exception("${result.statusCode}: ${result.reasonPhrase}");
    }
  }

  @override
  Future<bool> addFile(BarcodeObject obj, File file, Uint8List bytes, String descr)async {
    try {
      String fName = file.path.split("/").last;
      String fExt = fName.split(".").last;
      // fName = fName.substring(0, fName.length-fExt.length-1);
      String fSize = bytes.isEmpty ? fileSizeToStr(file.lengthSync()) : fileSizeToStr(bytes.length);

      print("Add file: ${file.path} - $fSize");

      // var bytes = file.readAsBytesSync();

      // await _post({
      //   "cmd": "addFile",
      //   "objectId": "${obj.id}",
      //   "fName": fName,
      //   "fExt": fExt,
      //   "fSize": fileSizeToStr(file),
      //   "descr": descr,
      //   "data": base64Encode(bytes)}
      // );
      // return true;

      var req = MultipartRequest("POST", cmdUri);
      req.fields['cmd'] = "addFile";
      req.fields['objectId'] = "${obj.id}";
      req.fields['fName'] = fName;
      req.fields['fExt'] = fExt;
      req.fields['fSize'] = fSize;
      req.fields['descr'] = descr;
      req.fields['session'] = session;
      req.fields['MAX_FILE_SIZE'] = "100000000";
      if(bytes.isEmpty) {
        req.files.add(await MultipartFile.fromPath("file", file.path)); //
      }else {
        req.files.add(await MultipartFile.fromBytes("file", bytes, filename: file.path));
      } //

      req.headers['cookie'] = cookie;

      var res = await req.send();
      print(res.stream.listen((value) {print(String.fromCharCodes(value));}));

      _getCookie(res.headers['set-cookie'] ?? "");

      if(res.statusCode < 300) {
        return true;
      }else{
        throw Exception("${res.statusCode}: ${res.reasonPhrase}");
      }
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
  }

  @override
  Future<int> addObject({String name = "", String barcode = ""}) async {
    try {
      var row = await _post({"cmd": "addObject", "name": name, "barcode": barcode, "projectId": "${curProject.id}", "userName": userInfo.name});
      return row;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return -1;
    }
  }

  @override
  Future<bool> checkObject(BarcodeObject obj)async {
    try {
      await _post({"cmd": "checkObject", "objectId": "${obj.id}", "userId": "${userInfo.id}", "barcode": obj.barcode});
      return true;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
  }

  @override
  Future<UserInfo> checkUser(String name, String pass)async {
    try {
      var res = await _post({"cmd": "checkUser", "name": name, "pass": pass});
      userInfo = UserInfo(id: res['Id'], name: res['Name']);
      session = res['session'] ?? "";
      return userInfo;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return UserInfo();
    }
  }

  @override
  Future<bool> checkUserGranted(objectId)async {
    try {
      var res = await _post({"cmd": "checkUserGranted", "objectId": "$objectId", "userId": "${userInfo.id}"});
      return res.length > 0;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
  }

  @override
  Future<Uint8List> fileData(int id)async {
    var result = await post(cmdUri, body: {"cmd": "fileData", "fileId": "${id}", "session": session}, headers: {'cookie': cookie});

    _getCookie(result.headers['set-cookie'] ?? "");

    if(result.statusCode < 300){
      return result.bodyBytes;
    }else{
      throw Exception(result.reasonPhrase);
    }
  }

  @override
  Future<List<FileInfo>> fileList(BarcodeObject obj) async {
    try {
      var res = await _post({"cmd": "fileList", "objectId": "${obj.id}"});
      List<FileInfo> list = [];
      for (var row in res) {
        list.add(FileInfo(
            row["Id"] ?? -1,
            row["Name"] ?? "",
            row["Ext"] ?? "",
            row["Description"] ?? "",
            row["FileSize"] ?? "",
            Uint8List.fromList([]), session));
      }
      return list;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<BarcodeObject> findObject(String barcode)async {
    try {
      var res = await _post({"cmd": "findObject", "barcode": barcode, "projectId": "${curProject.id}"});
      if(res.length>0) {
        var row = res[0];
        return BarcodeObject(
            id: row['Id'] ?? -1,
            name: row['Name'] ?? "",
            description: row['Description'] ?? "",
            foundDescr: "",
            //row['Found_Descr']
            barcode: row['Barcode'] ?? barcode,
            checkedTime: DateTime.parse("2000-01-01 00:00:00"),
            //row['CkeckedTime']
            isChecked: false //row['IsChecked']
            );
      }else{
        return BarcodeObject();
      }
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return BarcodeObject(id: -2);
    }
  }

  @override
  Future grantUser(BarcodeObject obj, bool checked)async {
    try {
      await _post({"cmd": "grantUser", "objectId": "${obj.id}", "userId": "${userInfo.id}", "userName": userInfo.name, "checked": checked ? "true" : "false", "barcode": obj.barcode});
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return BarcodeObject();
    }
  }

  @override
  Future<List<BarcodeObject>> objectsList()async {
    try {
      var res = await _post({"cmd": "objectList", "userId": "${userInfo.id}", "projectId": "${curProject.id}"});

      List<BarcodeObject> list = [];
      checkedCnt = 0;
      for(var row in res){
        var barcodeObj = BarcodeObject(
            id: row['Id_Obj'] ?? -1,
            name: row['Name'] ?? "",
            description: row['Description'] ?? "",
            foundDescr: row['Found_Descr'] ?? "",
            barcode: row['Barcode'] ?? "",
            checkedTime: DateTime.parse(row['CheckedTime'] ?? "2000-01-01 00:00:00"),
            isChecked: row['IsChecked'] ?? false
        );
        if(barcodeObj.isChecked){
          checkedCnt++;
        }
        list.add(barcodeObj);
      }
      return list;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<List<Project>> projectList()async {
    try {
      var res = await _post({"cmd": "projectList"});

      List<Project> list = [];
      for(var row in res){
        list.add(Project(
            id: row['Id'] ?? -1,
            name: row['Name'] ?? "",
            description: row['Description'] ?? ""
        ));
      }
      return list;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<bool> updateDescription(BarcodeObject obj)async {
    try {
      var res = _post({"cmd": "updateDescription", "object_id": "${obj.id}", "descr": "${obj.foundDescr}"});
      return true;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
  }

  void _getCookie(String coo) {
    cookie = coo.split(';')[0];
    print("cookie: $cookie");
  }

}

class DevHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}