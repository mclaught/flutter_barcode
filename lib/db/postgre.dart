import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_barcode/db/db.dart';
import 'package:postgres/postgres.dart';

class PostGREDB extends DB{

  Future<PostgreSQLConnection> _open()async{
    var conn = PostgreSQLConnection(host, port, db_name, username: user, password: pass);
    await conn.open();
    print("DB opened");
    return conn;
  }

  @override
  Future<UserInfo> checkUser(String name, String pass) async {
    try {
      var conn = await _open();

      String sql = """
      SELECT 
        barcode."Users"."Id", 
        barcode."Users"."Name", 
        "Description", 
        "Id_UserRoles", 
        "Id_Organizations", 
        "Document", 
        "Password",
        barcode."UserRoles"."Name" as "UserRole",
        barcode."UserRoles"."Id" as "UserRoleId"
      FROM 
        barcode."Users", 
        barcode."UserRoles"
      WHERE       barcode."Users"."Id_UserRoles"=barcode."UserRoles"."Id"
        and barcode."Users"."Name"='$name'
        and barcode."Users"."Password"='$pass';
    """;

      await conn.transaction((connection)async{
        List<List<dynamic>> results = await connection.query(sql);
        print(results);
        if (results.isNotEmpty) {
          var row = results[0];
          print(row);
          userInfo = UserInfo(id: row[0], name: row[1]);
          await _setSession(connection);
          return userInfo;
        }else{
          userInfo = UserInfo();
          return userInfo;
        }
      });
      return userInfo;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return UserInfo();
    }
  }

  Future _setSession(PostgreSQLExecutionContext connection)async{
    String sql = """
      SET SESSION usrvar.name ="${userInfo.name}";
    """;
    await connection.query(sql);
    print("Set session");
  }

  @override
  Future<List<Project>> projectList()async{

    try {
      var conn = await _open();

      String sql = """
      SELECT 
        "Id", "Name", "Description"
      FROM 
        barcode."Project";
    """;
      List<Map<String, Map<String, dynamic>>> results = await conn
          .mappedResultsQuery(sql);
      if (results.isNotEmpty) {
        projects = [];
        for (final row in results) {
          print(row);
          projects.add(Project(id: row['Project']!['Id'],
              name: row['Project']!['Name'] ?? '',
              description: row['Project']!['Description'] ?? ''));
        }
        return projects;
      }
      return [];
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<List<BarcodeObject>> objectsList()async{
    try {
      var conn = await _open();

      String sql = """
      SELECT 
        public."ZipEntities"."Id" as "Id_Obj", 
        "Id_Parent", 
        public."ZipEntities"."Name",
        public."ZipEntities"."Description", 
        "Id_ZipTypes", 
        "Id_Status", 
        "Count", 
        "Id_Dimentions", 
        "Id_Project", 
        public."ZipEntities"."Barcode",
        "Id_User",
        "IsChecked",
        barcode."CheckItem"."Barcode" as "BarCode_Found",
        "CheckedTime", 
        barcode."CheckItem"."Description" as "Found_Descr",
        barcode."Users"."Name" as "UserName"
      FROM 
        public."ZipEntities",
        barcode."CheckItem",
        barcode."Users"
      WHERE
      public."ZipEntities"."Id"=barcode."CheckItem"."Id_ZipEntities"
      and barcode."Users"."Id"=barcode."CheckItem"."Id_User"
      and "Id_Project"=${curProject.id}
      and "Id_User"=${userInfo.id}    
    """;
      checkedCnt = 0;
      List<List<dynamic>> results = await conn.query(sql);
      if (results.isNotEmpty) {
        objects = [];
        objectsIndex = {};
        for (final row in results) {
          // print(row);
          // print(row[6]);
          var barObject = BarcodeObject(
              id: row[0] ?? -1,
              name: row[2] ?? "",
              description: row[3] ?? "",
              barcode: row[9] ?? "",
              isChecked: row[11] ?? false,
              checkedTime: row[13] ?? DateTime.now(),
              foundDescr: row[14] ?? ""
          );
          if(barObject.isChecked) {
            checkedCnt++;
          }
          objects.add(barObject);
          objectsIndex[barObject.barcode] = barObject;
        }
        print("Checked: $checkedCnt");
        return objects;
      }
      return [];
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<BarcodeObject> findObject(String barcode)async{
    try {
      var conn = await _open();

      String sql = """
      SELECT * 
      FROM 
        public."ZipEntities"
      WHERE
        "Id_Project"= ${curProject.id}
        and "Barcode"= '${barcode}'    
    """;

      List<List<dynamic>> results = await conn.query(sql);
      for (final row in results) {
        print(row);
        return BarcodeObject(
            id: row[0] ?? -1,
            name: row[2] ?? "",
            description: row[3] ?? "",
            barcode: row[10] ?? ""
        );
      }

      return BarcodeObject();
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return BarcodeObject();
    }

    // if(!objectsIndex.containsKey(barcode)) {
    //   return BarcodeObject();
    // }else{
    //   return objectsIndex[barcode] ?? BarcodeObject();
    // }
  }

  @override
  Future<int> addObject({String name="", String barcode=""}) async {
    if(name.isEmpty || barcode.isEmpty)
      return -1;

    try {
      var conn = await _open();

      String sql = """
        INSERT INTO
        public."ZipEntities"
        (
          "Id_Parent",
          "Name",
          "Barcode",  
          "Description",
          "Id_ZipTypes",
          "Id_Status",
          "Count",
          "Id_Dimentions",
          "Id_Project"
        )
        VALUES 
        (
          0,
          '$name',
          '$barcode',
          '',
          1,
          4,
          1,
          7,
          ${curProject.id}
        );
      """;
      await conn.query(sql);

      sql = """
      SELECT 
        last_value 
      FROM 
        public."ZipEntities_Id_seq";
    """;
      List<List<dynamic>> results = await conn.query(sql);
      for (final row in results) {
        return row[0] ?? -1;
      }

      return -1;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return -1;
    }
  }

  @override
  Future<bool> checkUserGranted(objectId) async {
    try {
      var conn = await _open();

      String sql = """
      SELECT 
        "Id", 
        "Id_ZipEntities", 
        "Id_User", 
        "IsChecked",    
        "Barcode", 
        "CheckedTime", 
        "Description"
      FROM 
        barcode."CheckItem"
      WHERE
        "Id_ZipEntities"=$objectId and "Id_User"=${userInfo.id}
    """;

      List<List<dynamic>> results = await conn.query(sql);

      return results.length > 0;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
  }

  @override
  Future grantUser(BarcodeObject obj, bool checked)async {
    try {
      var conn = await _open();

      String sql = """
        INSERT INTO 
        barcode."CheckItem" 
        (
          "Id_ZipEntities",
          "Id_User",
          "IsChecked",
          "Barcode"
        )
        VALUES 
        (
          ${obj.id},
          ${userInfo.id},
          $checked,
          '${obj.barcode}'
        );
      """;

      await conn.query(sql);
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
    }
  }

  @override
  Future checkObject(BarcodeObject obj)async {
    try{
      var conn = await _open();

      String sql = """
        INSERT INTO 
        barcode."CheckItem" 
        (
          "Id_ZipEntities",
          "Id_User",
          "IsChecked",
          "Barcode"
        )
        VALUES 
        (
          ${obj.id},
          ${userInfo.id},
          true,
          '${obj.barcode}'
        )
        ON CONFLICT ON CONSTRAINT "Id_Zip_Id_User"
        DO UPDATE SET 
          "IsChecked"= true,
          "Barcode"= '${obj.barcode}';
      """;
      await conn.query(sql);
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
    }
  }

  Future<int> _addFileInfo(PostgreSQLExecutionContext connection, BarcodeObject obj, File file, String descr)async{
    String fName = file.path.split("/").last;
    String fExt = fName.split(".").last;
    fName = fName.substring(0, fName.length-fExt.length-1);
    print("$fName - $fExt");
    String sql = """
      INSERT INTO 
      public."Files"
      (
        "Id_ZipEntities", 
        "Name", 
        "Description", 
        "FileSize", 
        "Ext"
      )
      VALUES 
      (
        ${obj.id},
        '$fName',
        '$descr',
        '${fileSizeToStr(file.lengthSync())}',
        '.$fExt'
      )
      RETURNING "Id";
    """;

    var results = await connection.query(sql);
    if(results.length > 0){
      for(var row in results){
        return row[0];
      }
    }
    return -1;
  }

  Future _addFileData(PostgreSQLExecutionContext connection, int id, File file)async{
    var bytes = await file.readAsBytes();
    print("Bytes ${bytes.length}: ${bytes}");

    String sql = """
      INSERT INTO 
      public."FilesData"
      (
        "Id_Files", 
        "Data"
      )
      VALUES 
      (
        $id,
        decode(@bytes, 'base64')
      );
    """;

    // bytes = Uint8List.fromList([1,2,3,4,5,6,7,8,9,10]);
    String sBytes = base64Encode(bytes);

    await connection.query(sql, substitutionValues: {"bytes": sBytes});
  }

  @override
  Future<bool> addFile(BarcodeObject obj, File file, Uint8List bytes, String descr)async{
    try{
      var conn = await _open();

      await conn.transaction((connection)async{
        int id =  await _addFileInfo(connection, obj, file, descr);
        print("File ID = $id");
        if(id != -1){
          await _addFileData(connection, id, file);
          return true;
        }
      });
      return true;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return false;
    }
    return false;
  }

  @override
  Future<List<FileInfo>> fileList(BarcodeObject obj)async{
    try{
      var conn = await _open();

      String sql = """
        Select 
            public."Files"."Id", 
            public."Files"."Name", 
            public."Files"."Ext",
            public."Files"."Description", 
            public."Files"."FileSize"
        from public."Files" 
        where public."Files"."Id_ZipEntities" = ${obj.id};
      """;

      List<List<dynamic>> results = await conn.query(sql);

      // List<FileInfo> files = results.map<FileInfo>(
      //         (row)=>FileInfo(row[1] ?? "", row[2] ?? "", row[3] ?? "", row[4] ?? "", row[5] ?? [])
      // ).toList();
      List<FileInfo> files = [];
      for(dynamic row in results){
        files.add(FileInfo(row[0] ?? -1, row[1] ?? "", row[2] ?? "", row[3] ?? "", row[4] ?? "", Uint8List.fromList([]), ""));
      }
      return files;
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return [];
    }
  }

  @override
  Future<Uint8List> fileData(int id) async {
    Uint8List bytes = await loadFileCache(id);

    if(bytes.isNotEmpty){
      return bytes;
    }

    try{
      var conn = await _open();

      String sql = """
        Select "Data" from public."FilesData" where "Id_Files"=${id};
      """;

      List<List<dynamic>> results = await conn.query(sql);

      if(results.length>0){
        dynamic row = results[0];
        print("Bytes ${row[0].runtimeType} ${row[0].length} ${row[0]}");
        // bytes = base64Decode(String.fromCharCodes(row[0] ?? []));
        bytes = row[0] ?? [];
        saveFileCache(id, bytes);
        return bytes;
      }
      return Uint8List.fromList([]);
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
      return Uint8List.fromList([]);
    }
  }

  @override
  Future updateDescription(BarcodeObject obj)async {
    try{
      var conn = await _open();

      String sql = """
        UPDATE 
          barcode."CheckItem"
        SET 
          "Description"='${obj.foundDescr}'
        WHERE 
          "Id_ZipEntities"=${obj.id} 
           and  "Id_User"=${userInfo.id};
      """;

      await conn.query(sql);
    }catch(e,s){
      print(e);
      print(s);
      onErrorController.add(e.toString());
    }
  }

}
