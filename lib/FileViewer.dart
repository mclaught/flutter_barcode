import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode/db/db.dart';
import 'package:photo_view/photo_view.dart';

class FileViewer extends StatefulWidget{
  final FileInfo file;

  FileViewer(this.file);

  @override
  State<FileViewer> createState() => _FileViewState();

}

class _FileViewState extends State<FileViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.file.name),
      ),
      body: ImageTile(widget.file),
    );
  }}

class ImageTile extends StatelessWidget {
  final FileInfo file;

  ImageTile(this.file);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: PhotoView(
          imageProvider: file.imageProvider(),
          heroAttributes: PhotoViewHeroAttributes(
              tag: file.id
          ),
          initialScale: PhotoViewComputedScale.covered * 0.8,
          basePosition: Alignment.center,
          backgroundDecoration: BoxDecoration(color: Colors.black),
          errorBuilder: (context, _, __) => Image.asset("icon/image.png"),
        )
      ),
    );
  }
}