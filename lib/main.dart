import 'package:barcode_scan2/gen/protos/protos.pbenum.dart';
import 'package:barcode_scan2/model/scan_options.dart';
import 'package:barcode_scan2/platform_wrapper.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode/db/db.dart';
import 'package:flutter_barcode/db/webdb.dart';
import 'package:flutter_barcode/object_page.dart';
import 'package:flutter_barcode/projects.dart';
import 'package:flutter_barcode/scaners/scaner.dart';
import 'package:flutter_barcode/settings.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:package_info_plus/package_info_plus.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'db/postgre.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Barcode DB',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('ru', ''),
      ],
      theme: ThemeData(
        // primarySwatch: Colors.blue,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.blueGrey,
        ).copyWith(
            secondary: Colors.orange,
            background: Colors.blue.shade50
        )
      ),
      home: const MyHomePage(title: 'Barcode DB'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // DB db = PostGREDB();
  DB db = WebDB();
  String userName = "";
  String userPass = "";
  var _userController = TextEditingController(text: "test");
  var _passController = TextEditingController(text: "test");
  BarcodeObject currentObject = BarcodeObject();
  var _panelController = PanelController();
  List<String>? _codeTypes;
  late SharedPreferences _prefs;

  @override
  void initState() {
    db.onError.listen((error) {
      if(error.toString().contains("401:")){
        _auth();
        return;
      }

      showDialog(context: context, builder: (dlgCtx)=>AlertDialog(
        title: Text("Ошибка БД"),
        content: Text(error),
        actions: [
          TextButton(onPressed: (){
            Navigator.of(dlgCtx).pop();
          }, child: Text("Закрыть"))
        ],
      ));
      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text("Ошибка БД: $error"),
      // ));
    });
    _loadSetts();
    _checkUser();
  }

  _loadSetts()async{
    _prefs = await SharedPreferences.getInstance();
    _codeTypes = _prefs.getStringList("codeTypes");
  }

  @override
  Widget build(BuildContext context) {
    var scrSize = MediaQuery.of(context).size;

    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title + ": " + userInfo.name),
      //   actions: [
      //     IconButton(onPressed: ()=>selectProject(), icon: Icon(Icons.list))
      //   ],
      // ),
      body: SlidingUpPanel(
        body: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverAppBar(
              pinned: true,
              floating: true,
              snap: false,
              expandedHeight: 150,
              elevation: 5,
              forceElevated: true,
              title: Text(widget.title),
              actions: [
                IconButton(onPressed: () async {
                  var tmpCodeTypes = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => SettingsPage(_codeTypes ?? [])));
                  print("codeTypes = $tmpCodeTypes");
                  if(tmpCodeTypes != null) {
                    _codeTypes = tmpCodeTypes;
                        _prefs.setStringList("codeTypes", _codeTypes ?? []);
                      }
                    }, icon: Icon(Icons.settings_outlined)),
                IconButton(onPressed: (){
                  _auth();
                }, icon: Icon(Icons.account_circle_outlined)),
                IconButton(onPressed: ()=>_selectProject(), icon: Icon(Icons.list)),
                IconButton(onPressed: () async {
                  var info = await PackageInfo.fromPlatform();
                  showDialog(context: context, builder: (dlgCtx) => AboutDialog(
                      applicationName: "Barcode DB",
                      applicationIcon: Image.asset("icon/barcode_icon.png", width: 48, height: 48,),
                      applicationVersion: "${info.version} (${info.buildNumber})",
                    ));
                }, icon: Icon(Icons.help_outline),)
              ],
              flexibleSpace: FlexibleSpaceBar(
                // title: Text(userInfo.name + ":" + curProject.name),
                centerTitle: false,
                // background: Image.asset("icon/background.png", fit: BoxFit.fitWidth),
                background: Stack(
                  fit: StackFit.passthrough,
                  children: [
                    Image.asset("icon/background.png", fit: BoxFit.fitWidth),
                    Container(
                        margin: EdgeInsets.only(top: 30),
                        alignment: Alignment.center,
                        child: Text(db.curProject.name, style: TextStyle(fontSize: 32, color: Colors.white),)
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 20, bottom: 10),
                      alignment: Alignment.bottomRight,
                      child: Text(db.userInfo.name, style: TextStyle(fontSize: 20, color: Colors.white),),
                    ),
                  ],
                ),
              ),
            ),
            SliverList(delegate: SliverChildBuilderDelegate(
                  (context, index){
                var obj = db.objects[index];
                return ObjectTile(obj: obj, onTap: (obj)=>_showObject(obj));
              },
              childCount: db.objects.length
            ),)
          ],
        ),

        panel: Container(
          child: ObjectView(db, currentObject,
            showName: false,
            onPickFile: (file, bytes) async {
              await db.addFile(currentObject, file, bytes, '');
            },
            onDescrChanged: (String descr) async {
              currentObject.foundDescr = descr;
              currentObject.descrChanged = true;
            }
          ),
          margin: EdgeInsets.only(top: 30, left: 10, right: 10),
        ),

        header: Container(
          margin: EdgeInsets.only(top: 10),
          child: Image.asset("icon/handle.png",
            height: 50, width: scrSize.width, fit: BoxFit.fitWidth, alignment: Alignment.topCenter,
          ),
        ),
        // header: Container(
        //   alignment: Alignment.topCenter,
        //   width: scrSize.width,
        //   height: 40,
        //   decoration: BoxDecoration(color: Colors.red),
        //   child: Image.asset("icon/handle.png", height: 150, width: 400, fit: BoxFit.fill),
        // ),
        onPanelClosed: () async {
          if(currentObject.descrChanged) {
            await db.updateDescription(currentObject);
            setState(() {
              currentObject.descrChanged = false;
            });
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Описание сохранено"),
            ));
            // _updateObjects();

          }
          FocusScope.of(context).unfocus();
          setState(() {
            currentObject = BarcodeObject();
          });
        },
        padding: EdgeInsets.only(top: 0),
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        // minHeight: currentObject.id!=-1 ? 80 : 0,
        // maxHeight: currentObject.id!=-1 ? 300 : 0,
        minHeight: 0,
        maxHeight: 300,
        backdropEnabled: true,
        parallaxEnabled: false,
        controller: _panelController,
        color: Colors.white,
        boxShadow: [BoxShadow()],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _findObject,
        tooltip: 'Increment',
        child: Image.asset("icon/barcode.png", width: 24, height: 24,),//Icon(Icons.qr_code_scanner)
      ), // This trailing
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppBar(
        elevation: 25,
        shape: const CircularNotchedRectangle(),
        color: Theme.of(context).colorScheme.primary,
        clipBehavior: Clip.antiAlias,
        child: Container(
          padding: const EdgeInsets.all(24),
          child: Row(
            children: [
              Text("Количество: ${db.objects.length}", style: TextStyle(color: Colors.white),),
              Container(
                child: Text("Проверено: ${db.checkedCnt}", style: TextStyle(color: Colors.white),),
                margin: EdgeInsets.only(left: 20),
              ),
            ],
          )
        ),
      ),
    );
  }

  void _checkUser()async {
    await db.checkUser(userName, userPass);
    if (db.userInfo.id == -1) {
      _auth();
    } else {
      if (db.curProject.id == -1) {
        _selectProject();
      }
      setState(() {});
    }
  }

  void _auth()async{
    showDialog(context: context, builder: (dlgContext)=>AlertDialog(
      title: Text("Авторизация"),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(child: Text("Имя"), width: 100,),
              Expanded(
                child: TextField(
                  controller: _userController,
                ),
              )
            ],
          ),
          Row(
            children: [
              SizedBox(child: Text("Пароль"), width: 100,),
              Expanded(
                child: TextField(
                  controller: _passController,
                ),
              )
            ],
          ),
        ],
      ),
      actions: [
        TextButton(onPressed: (){
          userName = _userController.value.text;
          userPass = _passController.value.text;
          _checkUser();
          Navigator.of(dlgContext).pop();
        }, child: Text("Вход")),
        TextButton(onPressed: (){
          Navigator.of(dlgContext).pop();
        }, child: Text("Отмена")),
      ],
    ));
  }

  _selectProject()async {
    db.projects = await db.projectList();
    db.curProject = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProjectsPage(db.projects)));
    print(db.curProject.name);
    setState(() {});

    if(db.curProject.id != -1){
      _updateObjects();
    }
  }
  
  _updateObjects()async{
    db.objects = await db.objectsList();
    setState(() {});
  }

  Future<void> _findObjectByBarcode(String barcode)async{
    var object = await db.findObject(barcode);//BP-000002
    print("Object id = ${object.id}");
    if(object.id >= 0) {
      await _testUserGranted(object);
    }else if(object.id == -1){
      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Объект не найден")));
      showDialog(context: context, builder: (dlgCtx) => AlertDialog(
        title: Text(barcode),
        content: Text("Объект не найден. Создать новый объект?"),
        actions: [
          TextButton(onPressed: (){
            Navigator.of(dlgCtx).pop();
            _addObject(barcode);
          }, child: Text("Да")),
          TextButton(onPressed: (){
            Navigator.of(dlgCtx).pop();
          }, child: Text("Нет")),
        ],
      ));
    }
  }

  void _findObject()async {
    // await Permission.camera.request();
    //
    // var result = await BarcodeScanner.scan(options: ScanOptions(
    //   strings: {
    //     "cancel": "Отмена",
    //     "flash_on": "Вкл. вспышку",
    //     "flash_off": "Выкл. вспышку"
    //   }
    // ));
    // if(result.type != ResultType.Barcode)
    //   return;

    String barcode = await Scaner.create(_codeTypes ?? []).scan(context);
    if(barcode.isNotEmpty) {
      await _findObjectByBarcode(barcode);
    }
  }

  void _showObject(BarcodeObject object){
    setState(() {
      currentObject = object;
    });
    _panelController.open();

    // Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ObjectPage(object)));

    // showDialog(context: context, builder: (dlgContext)=>AlertDialog(
    //   title: Text(object.name),
    //   content: Container(child: ObjectView(object), height: 200, width: 400,),
    //   actions: [
    //     TextButton(onPressed: (){
    //       Navigator.of(dlgContext).pop();
    //     }, child: Text("OK"))
    //   ],
    // ));
  }

  Future _addObject(String barcode)async {
    var nameController = TextEditingController();
    showDialog(context: context, builder: (dlgCtx)=>AlertDialog(
      title: Text("Новый объект"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Название"),
          TextField(controller: nameController)
        ],
      ),
      actions: [
        TextButton(onPressed: () async {
          int newId = await db.addObject(name: nameController.value.text, barcode: barcode);
          if(newId != -1){
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Объект создан")));
          }else{
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Объект не создан")));
          }
          Navigator.of(dlgCtx).pop();
          _findObjectByBarcode(barcode);
        }, child: Text("Создать")),
        TextButton(onPressed: (){
          Navigator.of(dlgCtx).pop();
        }, child: Text("Отмена")),
      ],
    ));
  }

  Future<void> _testUserGranted(BarcodeObject obj) async {
    bool granted = await db.checkUserGranted(obj.id);

    bool checked = true;

    if(!granted){
      await showDialog(context: context, builder: (dlgCtx)=>AlertDialog(
        title: Text("Пользователь не назначен"),
        content: GrantUserContent(objName: obj.name, onChanged: (ch)=>checked=ch),
        actions: [
          TextButton(onPressed: () async {
            await db.grantUser(obj, checked);
            await _updateObjects();
            Navigator.of(context).pop();
          }, child: Text("Да")),
          TextButton(onPressed: (){
            Navigator.of(context).pop();
          }, child: Text("Нет")),
        ],
      ));
    }else{
      await db.checkObject(obj);
      obj.isChecked = true;
      obj.checkedTime = DateTime.now();
      await _updateObjects();
    }

    _showObject(obj);
  }
}

class GrantUserContent extends StatefulWidget {
  bool checked = true;
  String objName = "";
  Function(bool checked) onChanged;

  GrantUserContent({this.checked=true, this.objName="", required this.onChanged});

  @override
  State<GrantUserContent> createState() => _GrantUserContentState();
}

class _GrantUserContentState extends State<GrantUserContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text("Назначить пользователя на проверку объекта ${widget.objName}?"),
        Row(
          children: [
            Checkbox(value: widget.checked, onChanged: (bool? ch){
              setState(() {
                widget.checked = ch ?? false;
              });
              widget.onChanged(widget.checked);
            }),
            Expanded(child: Text("Отметить как проверенный"))
          ],
        )
      ],
    );
  }
}

class ObjectTile extends StatelessWidget {
  final BarcodeObject obj;
  final Function(BarcodeObject obj) onTap;

  const ObjectTile({Key? key, required this.obj, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: obj.isChecked ? Image.asset("icon/barcode_black.png") : Image.asset("icon/barcode_grey.png"),
      title: Text(obj.name),
      subtitle: Text(
          obj.isChecked ?
            "Проверено: ${DateFormat('dd-MM-yyyy kk:mm').format(obj.checkedTime ?? DateTime.fromMillisecondsSinceEpoch(0))}" :
            "Не проверено"
      ),
      onTap: (){
        onTap(obj);
      },
    );
  }
}
