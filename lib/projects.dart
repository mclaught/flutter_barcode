import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode/db/db.dart';

class ProjectsPage extends StatefulWidget{
  final List<Project> projects;

  ProjectsPage(this.projects);

  @override
  State<ProjectsPage> createState() => _ProjectPageState();
}

class _ProjectPageState extends State<ProjectsPage>  {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Проекты'),
      ),
      body: ListView(
        children: widget.projects.map((e) => Card(
          child: ListTile(
            leading: Image.asset("icon/project.png",),
            title: Text(e.name),
            subtitle: Text(e.description),
            onTap: (){
              Navigator.of(context).pop(e);
            },
          ),
        )).toList(),
      ),
    );
  }
}