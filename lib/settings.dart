import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget{
  Set<String> codeTypeSet = {};

  SettingsPage(List<String> codeTypes){
    codeTypeSet = codeTypes.toSet();
  }

  @override
  State<SettingsPage> createState() => _SettingsPageState();

}

class _SettingsPageState extends State<SettingsPage> {
  List<String> codeTypes = [
    "code_128",
    "ean",
    "ean_8",
    "code_39",
    "code_39_vin",
    "codabar",
    "upc",
    "upc_e",
    "i2of5",
    "2of5",
    "code_93"
  ];

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [
      Container(
        margin: EdgeInsets.only(top: 16, left: 20, bottom: 8),
        child: Text("Типы штрих-кодов",
          style: TextStyle(color: Theme.of(context).colorScheme.secondary, fontWeight: FontWeight.bold),
        ),
      )
    ];
    items.addAll(codeTypes.map<Widget>((t) => Row(
      children: [
        Checkbox(value: widget.codeTypeSet.contains(t), onChanged: (value){
          setState(() {
            if(value ?? false)
              widget.codeTypeSet.add(t);
            else
              widget.codeTypeSet.remove(t);
          });
        }),
        Expanded(child: Text(t))
      ],
    )).toList());

    return Scaffold(
      appBar: AppBar(
        title: Text("Настройки"),
        actions: [
          IconButton(icon: Icon(Icons.save_outlined), onPressed: (){
            Navigator.of(context).pop(widget.codeTypeSet.toList());
          },)
          // TextButton(child: Text("Сохранить"), onPressed: (){
          //   Navigator.of(context).pop(widget.codeTypes);
          // },)
        ],
      ),
      body: ListView(
        children: items,
      ),
    );
  }
}