import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode/FileViewer.dart';
import 'package:flutter_barcode/db/db.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

class FilesPage extends StatefulWidget{
  final DB db;
  final BarcodeObject object;

  FilesPage(this.db, this.object);

  @override
  State<FilesPage> createState() => _FilesPageState();

}

class _FilesPageState extends State<FilesPage> {
  List<FileInfo> files = [];
  bool loaded = false;

  @override
  void initState() {
    loadFiles();
  }

  void loadFiles()async{
    files = await widget.db.fileList(widget.object);
    setState(() {
      loaded = true;
    });
    for(var file in files){
      // widget.db.fileData(file.id).then((value){
      //   setState(() {
      //     file.bytes = value;
      //   });
      // });
    }
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    int colCnt = (screenSize.width/150).toInt();
    return Scaffold(
      appBar: AppBar(
        title: Text("Файлы"),
      ),
      body: loaded ? Container(
        margin: EdgeInsets.all(5),
        child: GridView.count(
          crossAxisCount: colCnt,
          children: files.map<Widget>((f) => Card(
            elevation: 5,
            child: GridTile(
              child: InkResponse(child: f.image(), onTap: () => _openFile(f),),
              footer: Container(
                padding: EdgeInsets.all(3),
                child: Column(
                  children: [
                    Text("${f.name}",//${f.ext}
                      maxLines: 1,
                      style: TextStyle(color: Colors.white, fontSize: 10),
                      overflow: TextOverflow.ellipsis
                    ),
                    Text("${f.size}",
                        maxLines: 1,
                        style: TextStyle(color: Colors.white, fontSize: 10),
                        overflow: TextOverflow.ellipsis
                    ),
                  ],
                ),
                decoration: BoxDecoration(color: Color.fromARGB(100, 0, 0, 0), ),
              ),
            ),
          )).toList(),
        ),
      ) : Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  _openFile(FileInfo f)async {
    if(!kIsWeb) {
      var bytes = await widget.db.fileData(f.id);
      Directory dir = await getTemporaryDirectory();
      print(dir);
      if(dir != null){
        File file = File(dir.path + "/${f.name}${f.ext}");
        await file.writeAsBytes(bytes);
        OpenFile.open(file.path);
      }
    }else{
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => FileViewer(f)));
    }
  }
}