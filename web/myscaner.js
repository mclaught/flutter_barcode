
function js_my_scan(codeTypes){
    console.info("codeTypes", codeTypes);

    var promise = new Promise((resolve, reject) => {
        console.log("My method test");
        console.info(document.querySelector('#camera_view'));

        var w = $(window).width();
        var h = $(window).height();
        var scanWin = window.open('scaner.html', 'scaner', 'width='+w+',height='+h);
        scanWin.onload = function() {
            scanWin.callback = function(barcode){
                console.info('callback', barcode);
                scanWin.close();
                resolve(barcode);
            };
            scanWin.initScan(codeTypes);
        };

    });

    console.info('promise', promise);

    return promise;
}
